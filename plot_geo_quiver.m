function plot_geo_quiver(ax, geo, field1, field2, other_geo, tind, plotscale, arrowscale)
%  plot_geo_quiver(ax, geo, field1, field2, other_geo, tind, plotscale, arrowscale)
%
% Plots (field1,field2) of geo as a quiver plot.
%
% If other_geo (on a sturctured grid) is given, reproject onto that.

% This m-file is part of the geofun package.
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

if ~exist('plotscale', 'var') || isempty(plotscale)
    plotscale = 1e3;
end
if ~exist('ax', 'var') || isempty(ax)
    figure();
    ax = axes();
end
if ~exist('other_geo', 'var') || isempty(other_geo)
    other_geo = [];
end

if ~exist('tind', 'var') || isempty(tind)
    tind = 1;
end
if ~exist('arrowscale', 'var') || isempty(arrowscale)
    arrowscale = 0;
end


if ~strcmp(geo.proj.type,'latlong')
    scalefac = plotscale;
else
    scalefac = 1;
end

if ~isempty(other_geo)
    % re-project
    geo = interp_geo_onto_other_geo(geo, other_geo, tind);
end

quiver(geo.X/scalefac, geo.Y/scalefac, geo.(field1)(:,:,tind), geo.(field2)(:,:,tind), arrowscale)
%n = 100;
%streamline(geo.X/scalefac, geo.Y/scalefac, geo.(field1)(:,:,tind), geo.(field2)(:,:,tind), -60*ones(n,1), linspace(-2260,-2100,n))

axis xy
axis equal