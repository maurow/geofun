function projs = standard_projections(type)
% projs = standard_projections(type)
%
% Returns a bunch of standard projections.  Also if you use another projection, add it
% here to document it.
%
% If type is give, only return the specific type:
% - 'none'
% - 'latlong'
% - 'bamber'  -- polar stereographic for Greenland as Bamber uses
% - 'gimp'  -- polar stereographic for Greenland as GIMP DEM uses
% - 'ant'   -- standard polar stereographic for Antarctica
%
% Returns a projs structure with fields containing some standard projections:
%
% - ps: polar stereographic:
%   - Bamber seaRise like
%   - GIMP like (aka ESPG 3413)
%   - Antarctica
% - latlong: lat long
% - none: no projection
%
% Note that for Greenland there are two standards floating around:
% - Bamber and seaRise use
%    std_parallel = 71
%    central_meridian = -39
% - GIMP and Joughin use
%    std_parallel = 70
%    central_meridian = -45

% This m-file is part of the geofun package.
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

% default fields for all proj
projdef.type = '';

% some default constants:
WGS84_earthradius = 6378137.0;
WGS84_eccentricity = 0.08181919;
                     
% polar stereo: bamber-like
proj = projdef;
proj.type = 'ps';
proj.subtype = 'Bamber & SeaRise -like';
proj.units = 'm';
proj.earthradius = WGS84_earthradius;
proj.eccentricity =  WGS84_eccentricity;
proj.std_parallel = 71;
proj.origin = 90;
proj.central_meridian = -39;
projs.ps.bamber = proj;

% polar stereo: gimp-like
proj = projdef;
proj.type = 'ps';
proj.subtype = 'GIMP-like';
proj.units = 'm';
proj.earthradius = WGS84_earthradius;
proj.eccentricity =  WGS84_eccentricity;
proj.std_parallel = 70;
proj.origin = 90;
proj.central_meridian = -45;
projs.ps.gimp = proj;

% polar stereo: Antarctica-like]
proj = projdef;
proj.type = 'ps';
proj.subtype = 'Antarctica';
proj.units = 'm';
proj.earthradius = WGS84_earthradius;
proj.eccentricity =  WGS84_eccentricity;
proj.std_parallel = -70;
proj.origin = -90;
proj.central_meridian = 0;
projs.ps.antarctica = proj;

% latlong
proj = projdef;
proj.units = 'deg';
proj.type = 'latlong';
projs.latlong = proj;

% lat-long rotated pole
% http://cf-pcmdi.llnl.gov/documents/cf-conventions/1.6/ch05s06.html\
proj = projdef;
proj.units = 'deg';
proj.type = 'rot_latlong';
proj.north_pole_lat = [];
proj.north_pole_long = [];
projs.rot_latlong = proj;

% UTM
proj = projdef;
proj.type = 'utm'; 
proj.zone = 'zone';
proj.units = 'm';
projs.utm = proj;

% CH1903
proj = projdef;
proj.type = 'ch'; 
projs.ch = proj;

% none
proj = projdef;
proj.units = 'm';
proj.type = 'none';
projs.none = proj;

% ...
proj = projdef;

if nargin>0
    switch type
      case 'none'
        projs = projs.none;
      case 'bamber'
        projs = projs.ps.bamber;
      case 'gimp'
        projs = projs.ps.gimp;
      case 'latlong'
        projs = projs.latlong;
      case {'ant', 'antarctica'}
        projs = projs.antarctica;
      case 'rot_latlong'
        projs = projs.rot_latlong;
      case 'utm'
        projs = projs.utm;
      case 'ch'
        projs = projs.ch;
      otherwise
        error('unknow projection')
    end
end
