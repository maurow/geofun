function [xy, bmark, bmark_edge] = interp_trajectory(geot, field, uu, mindist);
%  [xy, bmark, bmark_edge] = interp_trajectory(geot, field, uu, mindist);
%
% Returns points of the trajectory (given by geot.xy or geot.(field) if specified) at
% normailized pathlength points uu.  Mainly used for meshing the domain.  Note that the
% output will in general not have same length as uu.
%
% If mindist is given, then will error if any returned xy points are closer than mindist.
%
% If geot.diri and geot.flux exist, function will return bmark and bmark_edge as well.

% This m-file is part of the geofun package.
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file


if ~exist('field', 'var') || isempty(field)
    field = 'xy';
end

if ~exist('mindist', 'var') || isempty(mindist)
    mindist = [];
end

if size(uu,2)>1
    uu = uu';
end

if max(uu)>1 || min(uu)<0
    error('0<=uu<=1')
end

xy = geot.(field);

% path length
path_length = [0; cumsum(sqrt(diff(xy(:,1)).^2 + diff(xy(:,2)).^2))];
% scale to 1
path_length = path_length./path_length(end);

inds = [];
for iu = 1:length(uu)
    % find closest path_length to uu(iu)
    [~,ind] = min(abs(path_length - uu(iu)));
    inds(end+1) = ind;
end
inds = unique(inds);

if ~isempty(mindist)
    md = (xy(inds(2:end),:) - xy(inds(1:end-1),:)).^2;
    md = sqrt((sum(md,2)))
    if any(md<mindist)
        error('this function returns some points too closely spaced...')
    end
end


xy = xy(inds,:);

if isfield(geot, 'diri')
    bmark = geot.diri(inds);
    bmark_edge = 0*bmark;
    for ii = 2:length(bmark)
        % set bmark_edge to same as diri if between two equal diri nodes, otherwise to 0
        if bmark(ii-1)==bmark(ii)
            bmark_edge(ii-1) = bmark(ii); 
        else
            bmark_edge(ii-1) = 0;
        end
    end
    if bmark(1)==bmark(end) % close the loop
        bmark_edge(end) = bmark(end);
    end
end
    

