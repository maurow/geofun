function h = plot_geo(ax, geo, field, other_geo, tind, plotscale)
% plot_geo(ax, geo, field, other_geo, tind, plotscale)
%
% Plots geo as colorscale image
%
% If other_geo (on a sturctured grid) is given, reproject onto that.

% This m-file is part of the geofun package.
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

if ~exist('plotscale', 'var') || isempty(plotscale)
    plotscale = 1e3;
end
if ~exist('ax', 'var') || isempty(ax)
    figure();
    ax = axes();
end
% figure out default field:
if ~exist('field', 'var') || isempty(field)
    fields_ = fields(geo);
    nonofields = return_reserved_geo_fields();
    for i=1:length(fields_)
        if ~any(strcmp(fields_{i},nonofields))
            field = fields_{i};
            disp(['Plotting ', field])
            break
        end
    end
end

if ~exist('other_geo', 'var') || isempty(other_geo)
    other_geo = [];
end

if ~exist('tind', 'var') || isempty(tind)
    tind = 1;
end

if ~strcmp(geo.proj.type,'latlong')
    scalefac = plotscale;
else
    scalefac = 1;
end

if ~isempty(other_geo)
    % re-project
    geo = interp_geo_onto_other_geo(geo, other_geo, tind);
end

h = imagesc(geo.X(1,[1,end])/scalefac, geo.Y([1,end],1)/scalefac, geo.(field)(:,:,tind))

axis xy
axis equal