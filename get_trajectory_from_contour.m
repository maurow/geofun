function traj_geo = get_trajectory_from_contour(geo, field, level, minlength)
%  traj_geo = get_trajectory_from_contour(geo, field, level, minlength)
%
% Returns the trajectory defined as the longest contour line of geo.(field) at given
% level. (Mainly used to get the ice border from an ice thickness field.)  It then cleans
% up the contour by making sure that no two points are closer than minlength.
%

% This m-file is part of the geofun package.
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file
  

if ~exist('minlength') || isempty(minlength)
    minlength = geo.X(1,2) - geo.X(1,1);
end
    
val = geo.(field);

c = contourc(geo.X(1,:),geo.Y(:,1), val, [level,level]);

% go through contour matrix to find biggest piece of contour
maxlen = 0;
maxloc = 1;
curloc = 1;
while curloc<size(c,2)
    len = c(2,curloc);
    if len > maxlen
        maxlen = len;
        maxloc = curloc;
    end
    % update
    curloc = curloc + len + 1;
end
trajectory = (c(:,maxloc+1:maxloc+c(2,maxloc)))';

% tidy up #1: remove every other point if they are too close
md2 = minlength^2;
while true
    len = size(trajectory,1);
    topurge = logical(zeros(len,1));
    for i = 1:2:len-1
        d = sum((trajectory(i,:)-trajectory(i+1,:)).^2);
        if d<md2
            topurge(i+1) = true;
        end
    end
    if any(topurge)
        trajectory(topurge,:) = [];
    else
        break
    end
end

% tidy up #2: try to remove loops which are very slim
if size(trajectory,1)<100000
    % remove point which are too close to each other
    while true % needs itterating in some cases
        dist2 = dist2p(trajectory,minlength^2*2);  % find distances between points
        ind = triu(dist2<minlength^2, 1);
        ind = any(ind, 1);
        trajectory(ind,:) = [];
        if sum(ind)==0
            break;
        end
    end
else
    disp('Cannot clean up trajectory as it is too big.')
end

traj_geo = init_geo_struct(0, geo.proj);
traj_geo.xy = trajectory;

end % function
    
% $$$ function dist2=dist2p(xy, fill)
% $$$ % calcuates all squared distances between point in xy within +/- 10 points
% $$$     len = size(xy,1);
% $$$     dist2 = fill*ones(len, len);
% $$$     for ii=1:len
% $$$         for jj=1:10
% $$$             if ii+jj<1 || ii+jj>len
% $$$                 continue
% $$$             end
% $$$             dist2(ii,ii+jj) = sum((xy(ii,:)-xy(ii+jj,:)).^2);
% $$$         end
% $$$     end
% $$$ end

function dist2=dist2p(xy,unused)
% calcuates all squared distances between point in xy.
    len = size(xy,1);
    dist2 = zeros(len, len);
    for ii=1:len
        xx = repmat(xy(ii,:), len, 1);
        dist2(ii,:) = (sum((xx-xy).^2,2))';
    end
end

