function geo = resample_geo(geo, stride)
% geo = resample_geo(geo, stride)
%
% Drops every stride data-point as in var[1:stride:end, 1:stride:end].  Only works for
% structured 2D data.
%
% See also crop_nc.m which works directly on the netcdf file.

% This m-file is part of the geofun package.
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

non_data_fields = return_reserved_geo_fields();
fln = fieldnames(geo);
for ii =1:length(fln) % loop over variables
    field = fln{ii};
    if any(strcmp(field, non_data_fields))
        continue;
    end
    gf = geo.(field);
    if length(size(gf))==1
        error('Only works for structured data.')
        %        geo.(field) = gf(1:stride:end);
    elseif length(size(gf))==2
        geo.(field) = gf(1:stride:end, 1:stride:end);
    elseif length(size(gf))==3 % time dep
        error('Only works for structured data.')
    end
end

geo.X = geo.X(1:stride:end, 1:stride:end);
geo.Y = geo.Y(1:stride:end, 1:stride:end);