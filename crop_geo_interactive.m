function [geo_out, extent_out] = crop_geo_interactive(ax, geo, field, zoomfirst, plotscale) 
% [geo_out, extent_out] = crop_geo_interactive(ax, geo, field, zoomfirst, plotscale)
%
% Crops a rectangular region from geo-struct interactively.
%
% Interactive version of crop_geo.  Arguments:
% - ax  -- if axis handle, does not do any plotting (needs to be plotted in geo.proj)
%          if empty it will plot geo.field
% - geo -- geo struct
% - field -- field to plot
% - zoomfirst -- allow zooming frist (finish with "enter")
% - plotscale   -- if ax contains plot, setting this will descale extent
%
% Returns:
% - cropped geo
% - extent in geo.proj with which was cropped

% This m-file is part of the geofun package.
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

if ~exist('field', 'var') || isempty(field)
    field = [];
end
if ~exist('zoomfirst', 'var') || isempty(zoomfirst)
    zoomfirst = true;
end
if ~exist('plotscale', 'var') || isempty(plotscale)
    plotscale = 1;
end

msg = 'To crop select two opposite corners';

if ishandle(ax)
    axes(ax);
else 
    figure; ax = gca();
    plot_geo(ax, geo, field, [], [], plotscale)
    title(msg)
end
    
[x,y] = ginput_draw('k', 2, zoomfirst, msg);
extent = [x,y]*plotscale;
[geo_out, extent_out] = crop_geo(geo, extent);
% zoom to cropped
xlim([min(x), max(x)])
ylim([min(y), max(y)])
end

function sub = ret_sub(matrix, ind)
% returns the submatrix with inds. See
% http://stackoverflow.com/questions/11419220/matlab-extract-submatrix-with-logical-indexing
   sub = reshape(matrix(find(ind)), max(sum(ind)), max(sum(ind')));
end