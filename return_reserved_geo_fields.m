function non_data_fields = return_reserved_geo_fields()
% non_data_fields = return_reserved_geo_fields()
%
% Returns field names which are reserved fields, i.e. the ones which are not allowed to be
% used for data.
%
% Can be used to iterate over datafields:
%   non_data_fields = return_reserved_geo_fields()
%   fln = fieldnames(geo);
%   for ii =1:length(fln) % loop over variables
%       field = fln{ii};
%       if any(strcmp(field, non_data_fields))
%           continue;
%       end
%       ...
%   end

% This m-file is part of the geofun package.
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

non_data_fields = {'info', 'proj', 'xy', 'X', 'Y', 't'};