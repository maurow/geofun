function [x,y] = deg2ch(lon,lat)
%  [x,y] = deg2ch(lon,lat)
%
% Transforms Longitude,Latitude to Swiss CH1903 coordinates
%
% Adapted from:
% Source: http://www.swisstopo.admin.ch/internet/swisstopo/en/home/topics/survey/sys/refsys/projections.html (see PDFs under "Documentation")

% dec to seconds
sec_in_deg = 3600;
lat = lat*sec_in_deg;
lon = lon*sec_in_deg;

% Axiliary values (% Bern)
lat_aux = (lat - 169028.66) / 10000;
lon_aux = (lon - 26782.5) / 10000;

x = ((200147.07 + (308807.95 .* lat_aux) + ...
      + (3745.25 * lon_aux.^2) + ...
      + (76.63 * lat_aux.^2)) + ...
     - (194.56 * lon_aux.^2 .* lat_aux)) + ...
    + (119.79 * lat_aux.^3);

y = (600072.37 + (211455.93 * lon_aux)) + ...
    - (10938.51 * lon_aux .* lat_aux) + ...
    - (0.36 * lon_aux .* lat_aux.^2) + ...
    - (44.54 * lon_aux.^3);

% swap
tmp=x;
x=y;
y=tmp;