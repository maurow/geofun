function  [lon,lat] = ch2deg(x,y)
% [lon,lat] = ch2deg(x,y)
%
% Transforms Swiss CH1903 coordinates to Longitude, Latitude
%
% Adapted from:
% Source: http://www.swisstopo.admin.ch/internet/swisstopo/en/home/topics/survey/sys/refsys/projections.html (see PDFs under "Documentation")

% Axiliary values (% Bern)
x_aux = (x - 600000) / 1000000;
y_aux = (y - 200000) / 1000000;
lat = (16.9023892 + (3.238272 * y_aux)) + ...
      - (0.270978 * x_aux.^2) + ...
      - (0.002528 * y_aux.^2) + ...
      - (0.0447 * x_aux.^2 .* y_aux) + ...
      - (0.0140 * y_aux.^3);

% Convert CH y/x to WGS long
lon = (2.6779094 + (4.728982 * x_aux) + ...
       + (0.791484 * x_aux .* y_aux) + ...
       + (0.1306 * x_aux .* y_aux.^2)) + ...
      - (0.0436 * x_aux.^3);



%  seconds to deg
fac = 36/100;
lat = lat/fac;
lon = lon/fac;
