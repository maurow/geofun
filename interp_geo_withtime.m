function out = interp_geo_withtime(geo, field, x, y, time, proj, nan_val)
%  out = interp_geo_withtime(geo, field, x, y, time, proj, nan_val)
%
% Use for generic function for things in para.input:
% bed, surface topographie, velocity and others.
%
% Input
% geo  -- source geo-strcut
% field -- which field of geo-strcut to use
% x, y -- x, y coord of mesh points
% time -- time (defaults to [], for time independent things)
% proj -- projection of mesh
% nan_val -- if give replace any NaNs with this value
%
% Note this is just a super thin wrapper of interp_geo.m, except when there is time
% dependence.
%
% Time Dependence:
% 
% If geo has a field geo.t then the parameter time must be give.  The returned result is
% then the linear interpolation between time point.  Extrapolation is just the first or
% last time slice.

% This m-file is part of the geofun package.
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

if ~exist('time', 'var') || isempty(time)
    time = [];
end
if ~exist('proj', 'var') || isempty(proj)
    proj = geo.proj;
end

if ~exist('nan_val', 'var') || isempty(nan_val)
    nan_val = [];
end

% time independent stuff
if isempty(time) || ~isfield(geo, 't')
    % check
    if isfield(geo, 't') && length(geo.t)>1
        error('geo has time dependence.  Specify a time');
    end
    out = interp_geo(geo, field, x, y, [], proj); 
else
    % time dependence is a bit harder:
    
    % figure out indices corresponding to time
    ind1 = find(geo.t<=time, 1, 'last');
    ind2 = find(geo.t>time, 1, 'first');
    
    if isempty(ind1) % time before geo.t(1)
        if isempty(ind2)
            % this shouldn't happen
            error('something is iffy!');
        end
        out = interp_geo(geo, field, x, y, ind2, proj); 
    elseif isempty(ind2)
        % time after geo.t(end)
        out = interp_geo(geo, field, x, y, ind1, proj); 
    elseif ind1+1 ~= ind2
        % this shouldn't happen
        error('something is iffy!');
    else
        % linear interpolation between ind1 and ind2
        dt1 = time - geo.t(ind1) ;
        dt2 = geo.t(ind2) - time;
        w1 = dt2/(dt1+dt2);
        w2 = dt1/(dt1+dt2);
        
        out1 = interp_geo(geo, field, x, y, ind1, proj); 
        out2 = interp_geo(geo, field, x, y, ind2, proj); 
        out = w1*out1 + w2*out2;
    end
end

if ~isempty(nan_val)
    out(isnan(out)) = nan_val;
end