function [X,Y] = ginput_draw(c, npoints, zoom_first, msg)
% [X,Y] = ginput_draw(c, npoints, zoom_first, msg)
%
% Replicates ginput, but
% - drawing line segments between points with colour c
% - npoints -- read n points if specified
% - zoom_first -- if true let user zoom/pan/etc first (stop zooming by hitting enter)
% msg -- message

% This m-file is part of the geofun package.
% Copyright (c) 2014, Ian J Hewitt, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

if ~exist('c', 'var') || isempty(c)
    c = 'k';
end

if ~exist('npoints', 'var') || isempty(npoints)
    npoints = 10000000;
end
if ~exist('zoom_first', 'var') || isempty(zoom_first)
    zoom_first = false;
end
if ~exist('msg', 'var') || isempty(msg)
    msg = 'Click to enter points, finish with "enter".';
end

if zoom_first
    input('First zoom to region of interest, then hit "enter"','s');
end

disp(msg)

if ishold, holding = 1; else holding = 0; end
for i = 1:npoints
    [x,y] = ginput(1);
    if isempty(x),
        return;
    else
        hold on;
        h = plot(x,y,'.'); set(h,'color',c);
        if i==1, X = x; Y = y;
        else X = [X; x]; Y = [Y; y];
        h = plot([X(end-1) X(end)],[Y(end-1) Y(end)],'-'); set(h,'color',c);
        end
        if holding, else hold off; end
    end
    
end