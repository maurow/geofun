function [geo_out, extent_out] = crop_geo(geo, extent, proj_extent)
% [geo_out, extent_out] = crop_geo(geo, extent, proj_extent)
%
% Crops a rectangular region from geo-struct.  See also corp_nc.m if original files are
% netcdf.
%
% Arguments:
%    - first -- geo
%    - second is an extent in [x,y]. Either lower left & upper right 
%       (or any number of points of which min and max are taken)
%    - third the projection of the extent (if not identical to geo's)
%
% Returns:
% - cropped geo
% - extent in geo.proj with which was cropped
%
% There is also an interactive version of this function.

% This m-file is part of the geofun package.
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file
    
if ~exist('zoom_first', 'var') || isempty(zoom_first)
    zoom_first = false;
end
    
if ~exist('proj_extent', 'var') | isempty(proj_extent)
    transf_fn = get_transform();  % this returns the id function
else
    transf_fn = get_transform(proj_extent, geo.proj);
end

if size(extent,1)==2
    % make lower_right and upper_left corner as well
    extent2 =  [extent(:,1), extent(:,2); extent(:,1), extent(end:-1:1, 2)];
else
    extent2 = extent;
end
extent_out = [];
[extent_out(:,1),extent_out(:,2)] = transf_fn(extent2(:,1), extent2(:,2));
% max and min
minx = min(extent_out(:,1));
maxx = max(extent_out(:,1));
miny = min(extent_out(:,2));
maxy = max(extent_out(:,2));

geo_out = geo;
if isfield(geo,'X') % structured mesh
    nX = length(geo.X(1,:)); nY = length(geo.Y(:,1));
    %ind = geo.X>=minx & geo.X<=maxx & geo.Y>=miny & geo.Y<=maxy;
    xind = find(geo.X(1,:)>=minx & geo.X(1,:)<=maxx);
    if ~isempty(xind)
        xind = [max(xind(1)-1,1), xind, min(xind(end)+1,nX)];
    else
        xind = find(geo.X(1,:)>=minx);
        xind = [max(xind(1)-1,1), min(xind(1),nX)];
    end
    yind = find(geo.Y(:,1)>=miny & geo.Y(:,1)<=maxy);
    if ~isempty(yind)
        yind = [max(yind(1)-1, 1); yind; min(yind(end)+1,nY)];
    else
        yind = find(geo.Y(:,1)>=miny);
        yind = [max(yind(end),1), min(yind(end)+1,nY)];
    end
    ind = logical(geo.X*0);
    ind(yind,xind) = true;

    geo_out.X = ret_sub(geo.X,ind);
    geo_out.Y = ret_sub(geo.Y,ind);
else
    ind = geo.xy(:,1)>=minx & geo.xy(:,1)<=maxx & geo.xy(:,2)>=miny & geo.xy(:,2)<=maxy;
    geo_out.xy = geo.xy(ind,:);
end

fln = fieldnames(geo);
if isfield(geo, 't')
    time = geo.t;
else
    time = [1];
end
for ii =1:length(fln) % loop over variables
    field = fln{ii};
    if any(strcmp(field, return_reserved_geo_fields()));
        continue;
    end
    geo_out.(field) = [];
    for tt=1:length(time)
        if isfield(geo,'X') % structured mesh
            if isnumeric(geo.(field))
                geo_out.(field)(:,:,tt) = ret_sub(geo.(field)(:,:,tt),ind);
            end
        else
            if isnumeric(geo_out.(field))
                geo_out.(field)(:,tt) = geo.(field)(ind,tt);
            end
        end
    end
end
end

function sub = ret_sub(matrix, ind)
% returns the submatrix with inds. See
% http://stackoverflow.com/questions/11419220/matlab-extract-submatrix-with-logical-indexing
   sub = reshape(matrix(ind), max(sum(ind)), max(sum(ind')));
end