function transf_fn = get_transform(proj1, proj2);
%  transf_fn = get_transform(proj1, proj2);
%
% Returns a transformation function to transform coordinates in proj1 to coordinates in
% proj2.  The returned function has the following signature: 
%    [x2,y2] = transf_fn(x1,y1);
% which can be called with scalars, vectors or matrices.
%
% Can do transforms between:
% - polar stereo
% - latlong 
% - rotated latlong
%
% For
% - no-projection to no-projection (returns the identity function)
%
% Support for more projections can easily be added by providing two transformation
% functions: from and to lat-long.

% This m-file is part of the geofun package.
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

if nargin==0
    transf_fn = @id;
    return;
end
    
% if projections are the same, return the id function 
f1 = fields(proj1);
f2 = fields(proj2);
eq = true;
if length(f1)==length(f2)
    for ii=1:length(f1)
        if isfield(proj2, f1{ii})
            if strcmp(proj1.(f1{ii}),proj2.(f2{ii}))
                eq =  false;
                break
            end
        else
            eq =  false;
            break
        end
    end
else
    eq =  false;
end
if eq
    transf_fn = @id;
    return;
end    
            
% if they are not the same then figure out how to convert them

% pick transformation: proj1 -> lat-long
switch proj1.type
  case 'none'
    if proj2.type=='none'
        transf_fn = @id;
        return;
    else
        error(['Cannot tranform "none" to ', proj.type]);
    end
  case 'ps' % polar stereo
    t1 = @ps2ll_fn;
  case 'latlong' % lat-long
    t1 = @id;
  case 'rot_latlong' % rotated lat-long
    t1 = @rot2ll_fn;
  case 'utm'
    t1 = @utm2ll_fn;
  case 'ch'
    t1 = @ch2ll_fn;
  otherwise 
    error('unknow projection');
end

% pick transformation: lat-long -> proj2
switch proj2.type
  case 'ps' % polar stereo
    t2 = @ll2ps_fn;
  case 'latlong' % lat-long
    t2 = @id;
  case 'rot_latlong' % rotated lat-long
    t2 = @ll2rot_fn;
  case 'utm'
    t2 = @ll2utm_fn;
  case 'ch'
    t2 = @ll2ch_fn;
  otherwise 
    error('unknow projection');
end

transf_fn = @(x,y) compose_fns(x,y, t1, t2, proj1, proj2);

end % function

%%%%%%%%%%%%%%%%%%%%
% Transform functions.
%
% If transform is not identity, then the strategy is to transform:
% proj1 -> lat-long -> proj2

function [x,y] = compose_fns(x, y, t1, t2, p1, p2)
    [x,y] = t1(x, y, p1, p2); % now in lat-long
    [x,y] = t2(x, y, p1, p2); % now in new projection
end

%%%%%%%%%%%%%%%%%%%%
% Elementary transformation functions
function [x, y] = id(x, y, ~, ~)
% identity function
    x=x; y=y;
end

% polar stereographic
function [x, y] = ps2ll_fn(x, y, p1, ~)
    [x, y]= polarstereo_inv(x, y, p1.earthradius, p1.eccentricity, ...
                            p1.std_parallel, p1.central_meridian);
end
function [x, y] = ll2ps_fn(x, y, ~, p2)
    [x, y]= polarstereo_fwd(x, y, p2.earthradius, p2.eccentricity, ...
                            p2.std_parallel, p2.central_meridian);
end

% rotated lat-long
function [x,y] = rot2ll_fn(x, y, p1, ~)
    option = 2;
    % south pool lat/long
    SP_coord = [p1.north_pole_long-180, -p1.north_pole_lat];
    [x,y] = rotated_grid_transform(x, y, option, SP_coord);
end
function [x,y] = ll2rot_fn(x, y, ~, p2)
    option = 1;
    % south pool lat/long
    SP_coord = [p2.north_pole_long-180, -p2.north_pole_lat];
    [x,y] = rotated_grid_transform(x,y, option, SP_coord);
end

% UTM
function [x,y] = utm2ll_fn(x, y, p1, ~)
%    [x,y] = utm2deg(x,y,p1.zone);
    [x,y] = utm2deg(x,y,p1.zone);
end
function [x,y] = ll2utm_fn(x, y, ~, p2)
    [x,y,utmzone] = deg2utm(x,y);
end

% Swiss grid CH1903+
function [x,y] = ch2ll_fn(x, y, p1, ~)
    [x,y] = ch2deg(x,y);
end
function [x,y] = ll2ch_fn(x, y, ~, p2)
    [x,y] = deg2ch(x,y);
end
