function out = interp_geo(geo, field, x, y, tinds, proj, interp_meth, extrapval, throwerror)
% out = interp_geo(geo, field, x, y, tinds, proj, interp_meth, extrapval, throwerror)
%
% Return the data in geo.(field)(:,:,tind) at (x,y) in projection proj.  Also works for
% just interpolating data with no projection.
%
% Does this by transforming (x,y) to geo.proj and then interpolating.
%
% (x,y) points outside what is covered in geo will be NaN for a structured grid geo and
% ill defined otherwise.
%
% Input:
% - geo: input geo struct
% - field: field to transform.  If isempty(field) transform the whole geo struct and
%   return new geo strcut.
% - tinds: which time slices of (field) to interpolate on.  Defaults to [], which means all.
% - x,y: either vectors or meshgrid like arrays
% - proj: a projection structure. Defaults to the same as in geo.proj, thus rendering this
%   a simple interpolation function. (See standard_projections.m for all projections)
% - interp_meth: interpolation method, defaults to linear. See griddata and interp2.
% - extrapval: value assigned to points outside the domain (default NaN, if non-NaN no
%              warning will be given when points are outside)
% - throwerror: throws error if any points evaluate to NaN, otherwise only warn (default true)
%
% Output:
% - either data array of size(x) or geo struct

% This m-file is part of the geofun package.
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file
    
% get projection for (x,y) -> (geo.X,geo.Y)
if ~exist('proj', 'var') || isempty(proj)
    proj = geo.proj;
end
if ~exist('interp_meth', 'var') || isempty(interp_meth)
    interp_meth = 'linear';
end
if ~exist('extrapval', 'var') || isempty(extrapval)
    extrapval = NaN;
end
if ~exist('throwerror', 'var') || isempty(throwerror)
    throwerror = true;
end

% transformation function:
transf_fn = get_transform(proj, geo.proj);
% x and y in geo.proj
[xg, yg] = transf_fn(x, y);

if isempty(field) % return a geo struct
    fln = fieldnames(geo);
    % make new geo struct
    if any(size(x)==1) % not a meshgrid matrix
        out = init_geo_struct(false, proj);
        if size(x,1)>1
            out.xy = [x,y];
        else
            out.xy = [x',y'];
        end
    else
        out = init_geo_struct(true, proj);
        out.X = x;
        out.Y = y;
    end

    out.info = geo.info;
    out.info.comment = 'Reprojected & interpolated from original data';
    out.info.original_proj = geo.proj;

    non_data_fields = return_reserved_geo_fields();
    for ii =1:length(fln) % loop over variables
        field = fln{ii};
        if any(strcmp(field, non_data_fields))
            continue;
        end
        if ~exist('tinds', 'var') || isempty(tinds)
            tinds = 1:size(geo.(field),3);
        end
        for tt = 1:length(tinds)  % loop over time steps
            tind = tinds(tt);
            out.(field)(:,:,tt) = do_interp(geo, field, xg, yg, tind, interp_meth, extrapval, throwerror);
            if isfield(geo, 't')
                out.t(tt) = geo.t(tind);
            end
        end
    end
else % return a data array
    if ~exist('tinds', 'var') || isempty(tinds)
        tinds = 1:size(geo.(field),3);
    end
    for tt = 1:length(tinds)  % loop over time steps
        tind = tinds(tt);
        out(:,:,tt) = do_interp(geo, field, xg, yg, tind, interp_meth, extrapval, throwerror);
    end
end

end %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function out = do_interp(geo, field, xg, yg, tind, interp_meth, extrapval, throwerror)
    if isfield(geo, 'X') % if geo contains structured data
                         %interp_meth = ['*', interp_meth];  % faster for equally spaced data (which I assume here).
        out = interp2(geo.X, geo.Y, geo.(field)(:,:,tind), xg, yg, ['*',interp_meth], extrapval);
        if any(isnan(out))
            st = ['interp_geo.m: Some of the query points are NaNs or outside the domain of the input geo: ', inputname(1)];
            if throwerror
                error(st)
            else
                disp(' ')
                disp(st)
            end
           end
    else % unstructured
        out = griddata(geo.xy(:,1), geo.xy(:,2), geo.(field)(:,:,tind), xg, yg, interp_meth);
        if any(isnan(out))
            st = ['interp_geo.m: Some of the query points are NaNs or outside the domain of the input geo: ', inputname(1)];
            if throwerror
                error(st)
            else
                disp(' ')
                disp(['interp_geo.m: Some of the query points are NaNs or outside the domain of the input geo: ', inputname(1)])
            end
        end
    end
end