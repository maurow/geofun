function h = plot_geo_contour(ax, geo, field, other_geo, tind, levels, plotscale, props)
%  plot_geo_contour(ax, geo, field, other_geo, tind, levels, plotscale, props)
%
% Plots geo as contour map
%
% If other_geo is given, reproject onto that.
%
% props -- properties passed on to contour

% This m-file is part of the geofun package.
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

if ~exist('ax', 'var') || isempty(ax)
    figure();
    ax = axes();
end

if ~exist('plotscale', 'var') || isempty(plotscale)
    plotscale = 1e3;
end
if ~exist('other_geo', 'var') || isempty(other_geo)
    other_geo = [];
end

if ~exist('tind', 'var') || isempty(tind)
    tind = 1;
end
if ~exist('levels', 'var') || isempty(levels)
    levels = [];
end
if ~exist('props', 'var') || isempty(props)
    props = {};
end

% $$$ if ~exist('field', 'var') || isempty(field)
% $$$     field = getfield...
% $$$ end


if ~isempty(other_geo)
    % reproject
    geo = interp_geo_onto_other_geo(geo, other_geo, tind);
end

% $$$ cont = 100;
% $$$ contour(geo.X, geo.Y, geo.(field)(:,:,tind), [cont, cont])

if isempty(levels)
    [C,h] = contour(geo.X/plotscale, geo.Y/plotscale, geo.(field)(:,:,tind), props{:});
else
    [C,h] = contour(geo.X/plotscale, geo.Y/plotscale, geo.(field)(:,:,tind), levels, props{:});
end
axis xy
axis equal