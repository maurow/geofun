function geo = interp_geo_onto_other_proj(geo, other_proj, interp_meth)
%  new_geo = interp_geo_onto_other_geo(geo, other_proj, interp_meth)
%
% Returns an unstructured geo as this is the general case.

% This m-file is part of the geofun package.
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

if ~exist('interp_meth', 'var') || isempty(interp_meth)
    interp_meth = 'linear';
end

tfn = get_transform(geo.proj, other_proj);
if isfield(geo, 'xy') % unstructured
    [geo.xy(:,1), geo.xy(:,2)] = tfn(geo.xy(:,1), geo.xy(:,2));
    geo.proj = other_proj;
elseif isfield(other_geo, 'X') && isfield(geo, 'X') % structured
    [geo.xy(:,1), geo.xy(:,2)] = tfn(geo.X(:), geo.Y(:));
    geo.proj = other_geo;
    geo = rmfield(geo, 'X');
    geo = rmfield(geo, 'Y');
else
    error('When geo is on a structured grid, then this only works with structured grids for other_geo.');
end

