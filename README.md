# geofun

This package contains functions do deal with data in different
projections, presumably to get them all into the same model.  The
end-product is a function which can be queried at (x,y) for data, with
(x,y) in the projection of one's choice.  Thus this approach works
with both structured and unstructured grids.

I'm sure this package is reinventing the wheel, however, the nice
thing about this implementation is that it's simple and has no
dependencies.

Projections geofun can handle at the moment:

- lat-long
- rotated lat-long
- variants of polar stereographic
- none

However, it's easy to teach it to use new ones.

The package is based around storing geo-data along with their
projection in a structure (a geo-struct).  All the provided functions
work on those geo-structs.

## Install
Add this directory and `external` to the Matlab path.

## Demo
Probably easiest to get started with a demo: see the two demos in the
`example/` directory.

## Function reference
Here an overview of the functions.  For details see their help.

```
standard_projections        -- returns defined, standard projections
get_transform               -- returns a function which transforms (x,y) into
                               another projection.
init_geo_struct             -- returns an empty geo-struct: use for
                               initialisation.
interp_geo                  -- interpolate data onto new points, possibly in a
                               different projection.
interp_geo_withtime         -- same as interp_geo but it also does interpolation
                               between time slices
interp_geo_onto_other_geo   -- Interpolate geo-struct onto grid (and projection)
                               of another geo-struct.
interp_trajectory           -- Returns an interpolated trajectory (given by
                               geot.xy or geot.(field) if specified).
crop_geo                    -- Returns a cropped version of a geo-struct.
crop_geo_interactive        -- Interactive version of crop_geo
get_trajectory_interactive  -- Click on plot to make a trajectory
get_trajectory_from_contour -- Returns the trajectory defined as the
                               longest contour line of geo.(field) at given level.
closest_points_on_trajectory-- finds point of trajectory nearest to
                               specified point
plot_geo*                   -- various geo plotting functions
resample_geo                -- make geo-struct smaller by dropping some of
                               its points
return_reserved_geo_fields  -- returns a list of reserved fields of
                               the geo-struct
ginput_draw                 -- Helper function for clicking on plots
```

## Geodata Specs

Two structures are used in this package:

 - `proj-struct`: defines a projection.
 - `geo-struct`: holds geodata in a specific projection

## Projections
The m-file `standard_projections.m` has some standard_projections
defined, probably all that is needed.

`proj` is a structure with fields:
```
proj.info : info about projection
proj.type:   name of projection
proj.units  -- units
```

More fields may be defined depending on the particular projection.
No additional fields are defined for latlong and no projection.

For polar stereogaphic:
```
  proj.earthradius = 6378137.0 m (WGS84)
  proj.eccentricity =  0.08181919 (WGS84)
  proj.std_parallel = 70 or 71 (north) or -70 (south)
  proj.origin = 90 (north), -90 (south)
  proj.lon_posy = -39 or -45 (north), 0 (south)
```

For rotated lat-long:
```
proj.type = 'rot_latlong';
proj.north_pole_lat = [];
proj.north_pole_long = [];
```

## Geo data: geo-struct

The geo-struct holds the data.  Best is to get an empty geo-structure
by calling the function `init_geo_struct` and fill it with the data,
e.g.  loaded from a file.  See the m-file `example/load_data.m` for an
example.  There are some reserved fields in a geo-struct which are
returned by the function `return_reserved_geo_fields`, do not use
those for the data.

Any metadata goes into the field `.info`.


The all geo-struct have the fields:
```
geo.info        = metadata
geo.info.source = filename or description
geo.proj        = a projection structure 
```

Depending on whether the data is on a structure grid or not, there are
different geo-struct layouts.

### structured grid

```
geo.X   = x-coord (a meshgridded matrix)
geo.Y   = y-coord (a meshgridded matrix)

geo.val1 = value at X, Y
geo.val2 = value at X, Y
...

```

Example:
```
geoBed = 
           info: [1x1 struct]
           proj: [1x1 struct]
              X: [198x181 double]
              Y: [198x181 double]
            bed: [198x181 double]
           surf: [198x181 double]
       icethick: [198x181 double]
    delta_geoid: [198x181 double]
```

### unstructured grid, uses xy
```
geo.xy = 
geo.val1 = 
geo.val2 = 
...
```

### trajectory

xy gives trajectory other fields contain values along the
trajectory. Example: 
```
geoBorder = 
     info: [1x1 struct]
     proj: [1x1 struct]
       xy: [172x2 double]
    bmark: [172x1 double]
```

### time dependence

If data is time dependent then there is a field `t`.  Example:
```
geoSMB = 
      info: [1x1 struct]
      proj: [1x1 struct]
         X: [18x17 double]
         Y: [18x17 double]
         t: [2920x1 double]
    runoff: [18x17x2920 double]
```
