% A simple example

% This m-file is part of the geofun package.
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

addpath ..
addpath ../external/    

%% load data into geo-structs
[geoEle, geoGreen, geoSeaIce] = load_data();

%% some example processing

%% plotting of datasets in different projections

% get coastline from a contour of the landcover
geoGreenCoast = get_trajectory_from_contour(geoGreen, 'landcover', 2);

% plot sea ice data with Greenland coast line:
figure
plot_geo(gca, geoSeaIce, 'ice_conc', geoGreen) % plot geoSeaIce in projection of geoGreen 
hold on
plot_geo_trajectory(gca, geoGreenCoast)
title('Sea ice cover and Greenland coastline (from data with different projections).')

input('Press any key')
close

%% Compare two datasets in different projection and resolution:

% compare elevation in geoEle and geoGreen
geoEleGreen = interp_geo_onto_other_geo(geoEle, geoGreen); % interpolate onto grid of geoGreen

geoEleGreen.elevation(geoGreen.surface_ele==0) = 0;  % discard points below 0 meters
geoEleGreen.ele_diff = geoEleGreen.elevation-geoGreen.surface_ele;

figure
plot_geo(gca, geoEleGreen, 'ele_diff')
colorbar()
title('Difference between land elevation from TBASE (lat-long) and Bamber 2001 (polar stereo).')

input('Press any key')
close

%% Crop a geo-struct interactively

[geoGreen_cropped, extent_out] = crop_geo_interactive([], geoGreen, 'surface_ele', 1, 1e3);
title('Cropped geo-struct')

