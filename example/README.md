To run the demo:

- download example data with `download_data`
- run `demo`

# Notes

Any type of file, which Matlab can read, may be used even though all
the examples use NetCDF files (they are convenient).

# Data sources for examples

[Sea ice concentration around the North Pole](http://saf.met.no/p/ice/)
in a flavour of polar stereographic projection with
[this](ftp://osisaf.met.no/archive/ice/conc/2013/02/ice_conc_nh_polstere-100_multi_201302011200.nc)
specific dataset downloaded.

[Global elevation](http://jisao.washington.edu/data_sets/elevation/)
in lat-long, using [this dataset](http://jisao.washington.edu/data_sets/elevation/elev.0.25-deg.nc)

[SeaRise dataset](http://websrv.cs.umt.edu/isis/index.php/Present_Day_Greenland)
in another flavour of polar stereographic projection with
[this dataset downloaded](http://websrv.cs.umt.edu/isis/images/a/a5/Greenland_5km_v1.1.nc)

[Air temperature](http://www.esrl.noaa.gov/psd/data/gridded/data.crutem3.html),
long term mean from [HADCRU3](ftp://ftp.cdc.noaa.gov/Datasets/cru/hadcrut2/ltm/air.mon.ltm.nc)


Note: the way in which I combine above data does not make much sense,
it's for illustration only.
