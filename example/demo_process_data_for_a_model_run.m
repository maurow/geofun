% This example shows how the `getfun` package can be used to get data together for a
% model run.

% This m-file is part of the geofun package.
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

addpath ..
addpath ../external/    

% load the data
[geoEle, geoGreen, ~, geoAirTemp] = load_data();

% the ice-margin:
geoGreenIce = get_trajectory_from_contour(geoGreen, 'landcover', 3);

%% To save memory we CROP the data to where its needed
% Probably better to leave some margin

disp('Demo on getting different spatial data together for a model run.')
disp(' ')
disp('(1) Pick a box around the model domain.')
disp('Don''t make it too small because one dataset has a very coarse resolution.')
disp(' ')
figure; 
plot_geo(gca, geoGreen, 'surface_ele')
hold on
plot_geo_trajectory(gca, geoGreenIce)
title('Pick a box around the model domain. Don''t make it too small because one dataset is very coarse resolution.')
[~, extent_out] = crop_geo_interactive(gca, geoGreen, [], 0, 1e3);

geoEle = crop_geo(geoEle, extent_out, geoGreen.proj);
geoGreen = crop_geo(geoGreen, extent_out, geoGreen.proj);
geoAirTemp = crop_geo(geoAirTemp, extent_out, geoGreen.proj);
if any(size(geoAirTemp.X)<2)
    error('Picked too small a domain, not enough datapoints in geoAirTemp')
end
close

%% Make a model domain
% this is a irregular domain as one might use for finite element models or as a mask for
% structured gird models

figure; 
plot_geo(gca, geoGreen, 'surface_ele')
xlims = xlim(); ylims = ylim();
hold on
plot_geo_trajectory(gca, geoGreenIce)
xlim(xlims);ylim(ylims);
title('(2) Delineate model domain, it will automatically merge with ice margin')

disp(' ')
disp('Delineate model domain, it will automatically merge with ice margin')
disp(' ')
geoBoundary = get_trajectory_interactive(gca, geoGreenIce.proj, 0, 1e3);
close

% merge geoBoundary with ice-margin
inds = closest_points_on_trajectory(geoGreenIce, geoBoundary.xy([1,end],:));
if inds(1)<inds(2)
    geoBoundary.xy = [geoBoundary.xy; geoGreenIce.xy(inds(2):-1:inds(1),:)];
else
    geoBoundary.xy = [geoBoundary.xy; geoGreenIce.xy(inds(2):inds(1),:)];
end

figure; 
plot_geo(gca, geoGreen, 'surface_ele')
hold on
plot_geo_trajectory(gca, geoBoundary)
input('Finished!  Hit enter to continue to final plot.', 's');
close

%% END PRODUCT
% let's use another projection, just for fun:
end_proj = standard_projections('gimp'); 
surf_fn = @(x,y) double(interp_geo(geoEle, 'elevation', x, y, [], end_proj));
bed_fn = @(x,y) double(interp_geo(geoGreen, 'bed_ele', x, y, [], end_proj));
% this has time dependence too:
air_temp_fn = @(x,y,t) double(interp_geo_withtime(geoAirTemp, 'airtemp', x, y, t, end_proj));
boundary_fn = @(uu) interp_trajectory(geoBoundary, [], uu, 1e3); 

% if you need gridded data
tfn = get_transform(geoBoundary.proj, end_proj);
[bx,by] = tfn(geoBoundary.xy(:,1), geoBoundary.xy(:,2)); % boundary points in end_proj

x = linspace(min(bx), max(bx), 100);
y = linspace(min(by), max(by), 100);
[X,Y] = meshgrid(x,y);

t = 100:5:150;  % only interested in part of the year

ele = surf_fn(X,Y);
bed = bed_fn(X,Y);
temp =[];
for ii=1:length(t)
    temp(:,:,ii) = air_temp_fn(X,Y,t(ii));
end

%% plot the grid
% first rescale temp because of colormap annoyance of Matlab
mint = min(temp(:)); maxt = max(temp(:));
mine = min(ele(:)); maxe = max(ele(:));
temp_scaled = (temp+mint)/(maxt-mint)*(maxe-mine)+mine;
figure
imagesc(x,y,temp_scaled(:,:,6))
axis xy
hold
contour(X,Y,ele, 'w', 'linewidth', 1)
plot(bx([1:end,1]),by([1:end,1]),'k', 'linewidth',2)
title('Model domain, air temperature and surface topograpy all on the same grid in yet another projection.')
