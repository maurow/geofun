function [geoEle, geoGreen, geoSeaIce, geoAirTemp] = load_data()
% [geoEle, geoGreen, geoSeaIce, geoAirTemp] = load_data()
%
% Function which loads the example data into geo-structures. Geo-structs are then used to
% get the data in any projection.

% This m-file is part of the geofun package.
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

if ~exist('data', 'dir') 
    error('First download example data with download_data.')
end


%% a few constants
proj_latlong = standard_projections('latlong');
proj_polar1 = standard_projections('bamber');  % one flavour of polar stereographic projection

regular_grid = true;
fillval = NaN; % how missing data is represented

%% Global elevation dataset
ncfile = 'data/elevation.nc';
if ~exist(ncfile, 'file') 
    error('First download example data with download_data.')
end

% Use init_geo_struct to return an empty geo-struct
geoEle = init_geo_struct(regular_grid, proj_latlong);

lon =  double(squeeze(ncread(ncfile, 'lon')));
%!!! need longitude in range [-180,180]:
toshift = lon>180;
lon(toshift) = mod(lon(toshift),180) - 180;  
[lon, I] = sort(lon);
lat  =  double(squeeze(ncread(ncfile, 'lat')));
[X,Y] = meshgrid(lon,lat);
ele =  double(squeeze(ncread(ncfile, 'data'))');
ele(ele==32767) = fillval;
ele = ele(:,I);


geoEle.X = X;
geoEle.Y = Y;
geoEle.elevation = ele;  % field can have any name except what init_geo_non_data_fields() returns

% add some metadata
geoEle.info.file = ncfile;
geoEle.info.source = 'http://jisao.washington.edu/data_sets/elevation/';


%%  Greenland dataset
ncfile = 'data/greenland.nc';
if ~exist(ncfile, 'file') 
    error('First download example data with download_data.')
end

% use init_geo_struct to return an empty geo-struct
geoGreen = init_geo_struct(regular_grid, proj_polar1);

x =  double(squeeze(ncread(ncfile, 'x1')));
y  =  double(squeeze(ncread(ncfile, 'y1')));
[X,Y] = meshgrid(x,y);
landcover =  double(squeeze(ncread(ncfile, 'landcover'))');
surface_ele =  double(squeeze(ncread(ncfile, 'usrf'))');
ice_thickness =  double(squeeze(ncread(ncfile, 'thk'))');
bed_ele = surface_ele - ice_thickness;

geoGreen.X = X;
geoGreen.Y = Y;
geoGreen.surface_ele = surface_ele;
geoGreen.bed_ele = bed_ele;
geoGreen.ice_thickness = ice_thickness;
geoGreen.landcover = landcover;  

% add some metadata
geoGreen.info.file = ncfile;

%% Sea ice dataset
ncfile = 'data/sea_ice.nc';
if ~exist(ncfile, 'file') 
    error('First download example data with download_data.')
end

% this dataset uses a polar stereogrphic projection which is not in standard_projections.m
proj_polar2 = proj_polar1;
% modify:
proj_polar2.subtype = 'http://saf.met.no like';
proj_polar2.std_parallel = 70;
proj_polar2.earthradius = 6378273;
proj_polar2.central_meridian = -45;
proj_polar2.eccentricity =  sqrt( 1- 6356889.44891^2/ 6378273^2);

% use init_geo_struct to return an empty geo-struct
geoSeaIce = init_geo_struct(regular_grid, proj_polar2);

x =  double(squeeze(ncread(ncfile, 'xc')))*1e3; % convert to m
y  =  double(squeeze(ncread(ncfile, 'yc')))*1e3;
[X,Y] = meshgrid(x,y);
ice_conc =  double(squeeze(ncread(ncfile, 'ice_conc'))');
ice_conc(ice_conc==-999) = NaN;
confidence =  double(squeeze(ncread(ncfile, 'confidence_level'))');

geoSeaIce.X = X;
geoSeaIce.Y = Y;
geoSeaIce.ice_conc = ice_conc;
geoSeaIce.confidence = confidence;

% add some metadata
geoSeaIce.info.file = ncfile;


%% Air temperature

ncfile = 'data/airtemp.nc';
if ~exist(ncfile, 'file') 
    error('First download example data with download_data.')
end

% this dataset uses a polar stereogrphic projection which is not in standard_projections.m

% use init_geo_struct to return an empty geo-struct
geoAirTemp = init_geo_struct(regular_grid, proj_latlong);

x = double(squeeze(ncread(ncfile, 'lon')));
y = double(squeeze(ncread(ncfile, 'lat')));
t = double(squeeze(ncread(ncfile, 'time')));
[X,Y] = meshgrid(x,y);
airtemp =  double(squeeze(ncread(ncfile, 'air')));
airtemp = permute(airtemp, [2,1,3]);
airtemp(ice_conc==-9.96921e+36) = NaN;

geoAirTemp.X = X;
geoAirTemp.Y = Y;
geoAirTemp.t = t - min(t);
geoAirTemp.airtemp = airtemp;

% add some metadata
geoAirTemp.info.file = ncfile;
geoAirTemp.info.units_t = 'day of year';
