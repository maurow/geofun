% This downloads the example data from the internet.

% This m-file is part of the geofun package.
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

urls = {'ftp://osisaf.met.no/archive/ice/conc/2013/02/ice_conc_nh_polstere-100_multi_201302011200.nc',
        'http://jisao.washington.edu/data_sets/elevation/elev.0.25-deg.nc',
        'http://websrv.cs.umt.edu/isis/images/a/a5/Greenland_5km_v1.1.nc',
        'ftp://ftp.cdc.noaa.gov/Datasets/cru/hadcrut2/ltm/air.mon.ltm.nc'};
flns = {'sea_ice.nc', 'elevation.nc', 'greenland.nc', 'airtemp.nc'};

if ~exist('data', 'dir') 
    mkdir('data')
end

disp('Downloading example data (~30MB), see data/README.md for details.')
disp(' ')

try
    cd data
    for i =1:length(urls)
        disp(['Downloading: ', urls{i}])
        urlwrite(urls{i}, flns{i}, 'Timeout', 60);
    end
    cd ..
catch
    cd ..
end

