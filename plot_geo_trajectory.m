function plot_geo_trajectory(ax, geo, other_geo, plotscale)
%  plot_geo_trajectory(ax, geo, other_geo, plotscale)
%
% Plots geo.xy as trajectory
%
% If other_geo is given, reproject onto that.

% This m-file is part of the geofun package.
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

if ~exist('ax', 'var') || isempty(ax)
    figure();
    ax = axes();
end

if ~exist('other_geo', 'var') || isempty(other_geo)
    other_geo = geo; % give id-transformation
end

if ~exist('plotscale', 'var') || isempty(plotscale)
    plotscale = 1e3;
end
% re-project
fn = get_transform(geo.proj, other_geo.proj);
[xy(:,1), xy(:,2)] = fn(geo.xy(:,1), geo.xy(:,2));

plot(xy(:,1)/plotscale, xy(:,2)/plotscale, 'g.-')
%plot(xy(:,1)/plotscale, xy(:,2)/plotscale, 'kx', 'markersize', 14 )
