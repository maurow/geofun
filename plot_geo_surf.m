function plot_geo_surf(ax, geo, field, other_geo, tind)
% plot_geo(ax, geo, field, other_geo, tind)
%
% Plots geo as colorscale image
%
% If other_geo is given, reproject onto that.

% This m-file is part of the geofun package.
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

if ~exist('ax', 'var') || isempty(ax)
    figure();
    ax = axes();
end

if ~exist('other_geo', 'var') || isempty(other_geo)
    other_geo = [];
end

if ~exist('tind', 'var') || isempty(tind)
    tind = 1;
end

% $$$ if ~exist('field', 'var') || isempty(field)
% $$$     field = getfield...
% $$$ end


if ~isempty(other_geo)
    % reproject
    geo = interp_geo_onto_other_geo(geo, other_geo, tind);
end

surf(geo.X, geo.Y, geo.(field)(:,:), 'edgealpha', 0)
axis xy
axis equal