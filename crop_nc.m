function ncoutfile = crop_nc(ncfile, ncoutfile, extent_inds, extent_coords, stride, overwrite, xyname, throwerror)
%  ncoutfile = crop_nc(ncfile, ncoutfile, extent_inds, extent_coords, stride, overwrite, xyname, throwerror)
%
% Uses the ncks NetCDF command-line tool [1] to crop & resample a 2D netcdf file (this
% means ncks needs to be installed).  This is a lot faster and memory efficient than
% loading the whole file into Matlab and crop it with crop_geo.m.
%
% Input:
%  ncfile -- input file
%  ncoutfile -- output file.  if [] is passed in ncfile.crop_x1_x2_y1_y2.nc is used, where
%               x1 etc are the indices of the crop.
%  extent_inds/coords -- the extent of the crop, either in indices or coordinates. [x1,y1,x2,y2]
%  stride -- stride to take as in x[1:stride:end] [1]
%  overwirte  -- overwrite existing file if == 1, error if ==-1, do nothing if == 0 [0]
%  xyname -- names of the x and y coordinate. {'x', 'y'}
%  throwerror -- if true throw error if ncks fails [true]
%
% Returns:
%  ncoutfile -- name of file written to
%
% Resources:
% [1] http://linux.die.net/man/1/ncks
% https://stackoverflow.com/questions/25731393/is-there-a-way-to-crop-a-netcdf-file

% This m-file is part of the geofun package.
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

if ~exist('throwerror', 'var') || isempty(throwerror)
    throwerror = true;
end
if ~exist('overwrite', 'var') || isempty(overwrite)
    overwrite = 0;
end

if ~exist('xyname', 'var') || isempty(xyname)
    xyname = {'x', 'y'};
end
xn = xyname{1};
yn = xyname{2};

if ~exist('extent_inds', 'var') || isempty(extent_inds)
    extent_inds = [];
else
    % reorder
    extent_inds = extent_inds(:);
end
if ~exist('extent_coords', 'var') || isempty(extent_coords)
    extent_coords = [];
else
    % reorder
    extent_coords = extent_coords(:);
end
if ~exist('stride', 'var') || isempty(stride)
    stride = 1;
end

if isempty( extent_coords) & isempty(extent_inds) & isempty(stride) 
    error('One of extent_coords, extent_inds or stride needs to be specifed')
elseif ~isempty( extent_coords) & ~isempty(extent_inds)
    error('Only one of extent_coords or extent_inds can be specifed')
end

if ~isempty(extent_coords)
    % translate coordinates to indices
    xy =  {double(squeeze(ncread(ncfile, xn))), double(squeeze(ncread(ncfile, yn)))};
    ii = 1;
    for i=1:2
        c = xy{i};
        if c(1)<c(2) % increasing
            extent_inds(ii) = max(find(c<extent_coords(ii)));
            ii = ii+1;
            extent_inds(ii) = min(find(c>extent_coords(ii)));
            ii = ii+1;
        else
            extent_inds(ii+1) = min(find(c<extent_coords(ii)));
            ii = ii+1;
            extent_inds(ii-1) = max(find(c>extent_coords(ii)));
            ii = ii+1;
        end            
    end
elseif isempty( extent_coords) & isempty(extent_inds)
    % find length of vectors
    x = double(squeeze(ncread(ncfile, xn)));
    y = double(squeeze(ncread(ncfile, yn)));
    extent_inds = [1, length(x), 1, length(y)];
end

% make output file-name
if isempty(ncoutfile)
    sinds = strjoin(strsplit(num2str(extent_inds)), '_');
    ncoutfile = [ncfile, '.crop_', sinds, '.nc'];
end
if exist(ncoutfile, 'file')
    if overwrite==-1
        error(['Output file ', ncoutfile, ' exists.'])
    elseif overwrite==0
%        disp('Doing nothing')
        return % do nothing
    end
end

% https://stackoverflow.com/questions/25731393/is-there-a-way-to-crop-a-netcdf-file
cmd = sprintf('ncks -d %s,%d,%d,%d -d %s,%d,%d,%d %s -O %s', xn, extent_inds(1), extent_inds(2), stride, yn, extent_inds(3), extent_inds(4), stride, ncfile, ncoutfile);
%disp(cmd)

[status,result] = unix(cmd);
if status~=0 && throwerror
    disp(cmd)
    disp(status)
    error(result)
end
