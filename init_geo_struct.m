function geo = init_geo_struct(reggrid, proj);
% geo = init_geo_struct(reggrid, proj);
%
% Returns an empty geo struct.  Add data at x,y points as fields of any name.  Do not add
% non-data fields to geo only to geo.info.
%
% reggrid=true for a regular grid with meshgrid X,Y
% reggrid=false for a unstructured grid with xy coords
%
% For time dependent fields add a time variable geo.t
%
% For trajectories use only geo.xy, no data.
%
% proj -- projection.  Defaults to 'none'

% This m-file is part of the geofun package.
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

if nargin<1
    reggrid = false;
end
if nargin<2
    proj = standard_projections('none');
end

geo.info.source = ''; % description of data
geo.info.file = ''; % filename of original data
% put anything extra fields into geo.info, do not add more fields to geo itself!

geo.proj = proj; % a projection instance

if reggrid  % structured mesh: i.e. meshgrid X and Y matrices
    geo.X   = []; %x-coord (a meshgridded matrix)
    geo.Y   = []; %y-coord (a meshgridded matrix)

    % Data would be stored like:
    %      geo.val1 = value at X, Y
    %      geo.val2 = value at X, Y
else  % unstructured mesh: i.e. a bunch of points
    geo.xy = [];

    % Data would be stored like:
    %      geo.val1 = at xy
    %      geo.val2 = 
end

