function geo = interp_geo_onto_other_geo(geo, other_geo, tinds, interp_meth)
%  new_geo = interp_geo_onto_other_geo(geo, other_geo, tinds, interp_meth)
%
% For unstructured data in geo:
%   - Transform geo to projection of other_geo
% For structured data in geo and other_geo:
%   - Interpolate geo onto grid (and projection) of other geo.

% This m-file is part of the geofun package.
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

if ~exist('tinds', 'var') || isempty(tinds)
    tinds = [];
end

if ~exist('interp_meth', 'var') || isempty(interp_meth)
    interp_meth = 'linear';
end

if isfield(geo, 'xy') % unstructured
    tfn = get_transform(geo.proj, other_geo.proj);
    [geo.xy(:,1), geo.xy(:,2)] = tfn(geo.xy(:,1), geo.xy(:,2));
    geo.proj = proj;
elseif isfield(other_geo, 'X') && isfield(geo, 'X') % structured
    geo = interp_geo(geo, [], other_geo.X, other_geo.Y, tinds, other_geo.proj, interp_meth);    
else
    error('When geo is on a structured grid, then this only works with structured grids for other_geo.');
end

