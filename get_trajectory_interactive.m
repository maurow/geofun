function geo = get_trajectory_interactive(ax, proj, zoomfirst, plotscale)
%  geo_traj = get_trajectory_interactive(ax, proj, zoomfirst, plotscale)
%
% Define a trajectory by clicking.
%
% ax -- axes where clicking is done
% proj -- projection of plot and trajectory
% - zoomfirst -- allow zooming frist (finish with "enter")
% - plotscale   -- if ax contains plot, setting this will descale extent

% This m-file is part of the geofun package.
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

if ~exist('proj', 'var') || isempty(proj)
    proj = standard_projections('none');
end
if ~exist('plotscale', 'var') || isempty(plotscale)
    plotscale = 1;
end
if ~exist('zoomfirst', 'var') || isempty(zoomfirst)
    zoomfirst = true;
end

axes(ax);
[x,y] = ginput_draw('k', [], zoomfirst);
geo = init_geo_struct(0, proj);
geo.xy = [x,y]*plotscale;
