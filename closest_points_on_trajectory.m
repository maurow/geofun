function inds = closest_points_on_trajectory(geo, points)
%  ind = closest_points_on_trajectory(geoTrajcetory, points)
%
% Returns the logical index array of the points of the trajectory in geoTrajectory which
% are closest to points.
    
% This m-file is part of the geofun package.
% Copyright (c) 2014, Mauro A Werder
% All rights reserved.
% Licensed under a BSD 2-Clause License, see LICENCE file

inds = [];
for ii=1:size(points,1)
    [~,ind] = min(dist2p(geo.xy, points(ii,:)));
    inds(end+1) = ind;
end
end

function dists=dist2p(xy, pt)
% calcuates squared distances between point pt and xy.
    len = size(xy,1);
    pt = repmat(pt, len, 1);
    dists = (sum((pt-xy).^2,2))';
end
